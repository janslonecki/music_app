CarrierWave.configure do |config|

  # Use local storage if in development or test
  if Rails.env.development? || Rails.env.test?
    CarrierWave.configure do |config|
      config.storage = :file
    end
  end

  # Use AWS storage if in production
  if Rails.env.production?
    CarrierWave.configure do |config|
      config.storage = :fog
    end
  end

  config.fog_provider = 'fog/aws'                        # required
  config.fog_credentials = {
    provider:              'AWS',                        # required
    aws_access_key_id:     'AKIAI5TQOI2M4IY3MWKA',                        # required
    aws_secret_access_key: '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',                        # required
    region:                'eu-central-1',                  # optional, defaults to 'us-east-1'
  }
  config.asset_host = "http://d3bs64f5l8hf4s.cloudfront.net"
  config.fog_directory  = 'tapecamp'                          # required
  config.fog_public     = true                                        # optional, defaults to true
  config.fog_attributes = { 'Cache-Control' => "max-age=#{365.day.to_i}" } # optional, defaults to {}
end