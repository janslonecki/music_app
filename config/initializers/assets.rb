# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( logoxm.png )

Rails.application.config.assets.precompile += %w( logosm )

Rails.application.config.assets.precompile += %w( customstyles.scss )

Rails.application.config.assets.precompile += %w( bar-ui.css )

Rails.application.config.assets.precompile += %w( soundmanager2.js )

Rails.application.config.assets.precompile += %w( logoxmmini )

Rails.application.config.assets.precompile += %w( search )

Rails.application.config.assets.precompile += %w( searchtoggle )

Rails.application.config.assets.precompile += %w( bar-ui.js )

Rails.application.config.assets.precompile += %w( texture2.png )

Rails.application.config.assets.precompile += %w( progball )

Rails.application.config.assets.precompile += %w( progballbl )


# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
