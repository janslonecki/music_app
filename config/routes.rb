Rails.application.routes.draw do

devise_for :artists, :controllers => { :registrations => 'artists/registrations' }

resources :artists do
resources :mixtapes do
	resources :songs
    get :autocomplete_mixtape_title, :on => :collection
	member do
		put "upvote", to: "mixtapes#upvote"
		put "downvote", to: "mixtapes#downvote"
end

get '/download', to: 'mixtapes#download'
end
end

resources :songs

resources :mixtapes

root 'mixtapes#home'

get '/download', to: 'mixtapes#download'

get '/singles', to: 'songs#index'

get "search/query"

get 'search/results', to: 'search#results'

end
