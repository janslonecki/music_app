class AddArchiveToMixtapes < ActiveRecord::Migration
  def change
    add_column :mixtapes, :archive, :string
  end
end
