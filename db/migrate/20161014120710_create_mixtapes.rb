class CreateMixtapes < ActiveRecord::Migration
  def change
    create_table :mixtapes do |t|
      t.string :title
      t.integer :tracksamount
      t.integer :downloadscount
      t.references :artist, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
