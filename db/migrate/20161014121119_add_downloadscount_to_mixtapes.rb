class AddDownloadscountToMixtapes < ActiveRecord::Migration
  def change
    add_column :mixtapes, :downloadscount, :integer, :default => 0
  end
end
