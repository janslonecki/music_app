class RemoveDownloadscountFromMixtapes < ActiveRecord::Migration
  def change
    remove_column :mixtapes, :downloadscount, :integer
  end
end
