class Song < ActiveRecord::Base
  belongs_to :mixtape
  mount_uploader :audio, AudioUploader
  validates_presence_of :audio

end
