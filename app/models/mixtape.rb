class Mixtape < ActiveRecord::Base
belongs_to :artist
has_many :songs, dependent: :destroy
	mount_uploader :mixtape_av, MixtapeAvUploader
	mount_uploader :archive, ArchiveUploader
has_many :comments
acts_as_votable
is_impressionable

searchable do
	text :title
end

accepts_nested_attributes_for :songs

end
