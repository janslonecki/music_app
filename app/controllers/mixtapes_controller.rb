class MixtapesController < ApplicationController
before_action :find_mixtape, only: [:destroy]
impressionist :actions=>[:show]
 include ActionController::Streaming
 include Zipline

def index
    @search = Mixtape.search do
        fulltext params[:search]
    end
	@mixtapes = @search.results
end

def home
    @mixtapes = Mixtape.all
    
end
 
def new
    @artist = current_artist
	  @mixtape = @artist.mixtapes.new
    @mixtape.mixtape_av = params[:file]

end

   def create
    @artist = current_artist
    @mixtape = @artist.mixtapes.create(mixtape_params)

    if @mixtape.save
        redirect_to edit_artist_mixtape_path(@artist, @mixtape)
    else
        render 'new'
    end
   end


   def destroy
      @mixtape.destroy
      redirect_to artist_path(@artist)
   end


def strona
  
end

def show
	@mixtape = Mixtape.find(params[:id])
    @artist = @mixtape.artist
    @artistsmixtapes = @artist.mixtapes.all.where.not(id: @mixtape.id)
    @songs = @mixtape.songs.all
    @background = @mixtape.mixtape_av.url
    impressionist(@mixtape)
    respond_to do |format|
      format.html
      format.js
    end

end


def edit
    @mixtape = Mixtape.find(params[:id])
    @songs = @mixtape.songs.all
    @tracksamount = @mixtape.tracksamount
end

def update
    @mixtape = Mixtape.find(params[:id])
    if @mixtape.update(mixtape_params)
       redirect_to mixtape_path(@mixtape)
        else
        render 'new'
    end
end

def load_suggestions
  @suggestions = Mixtape.select(:title) or Mixtape.find(:all) #Select the data you want to load on the typeahead.
  render json: @suggestions
end


def upvote
    @mixtape = Mixtape.find(params[:id])
    @mixtape.upvote_by current_user
    redirect_to mixtape_path(@mixtape)
end

def downvote
    @mixtape = Mixtape.find(params[:id])
    @mixtape.downvote_by current_user
    redirect_to mixtape_path(@mixtape)
end


def download
  @mixtape = Mixtape.find(params[:mixtape_id])
  url = @mixtape.archive.url
  @mixtape.downloadscount += 1
  @mixtape.save
    redirect_to(url)
end

    private

        def mixtape_params
            params.require(:mixtape).permit(:id, :title, :artist_id, :mixtape_av, :archive, :downloadscount, :tracksamount, songs_attributes: [:id, :title, :trackid, :audio])
        end

        def find_mixtape
            @mixtape = Mixtape.find(params[:id])
        end



        def find_radio
            @mixtape = Mixtape.last
        end

        def song_params
            params.require(:song).permit(:title, :trackid, :audio)     
        end

end