class SongsController < ApplicationController
  before_action :find_song, only: [:destroy]
  before_action :find_mixtape, only: [:create, :destroy]


   def index
    @allsongs = Mixtape.all.songs.all
   end

   def create
   	@mixtape = Mixtape.find(params[:mixtape_id])
   	@song = @mixtape.songs.create(song_params)
    @song.audio = params[:fog]

   	if @song.save
   		redirect_to edit_mixtape_path(@mixtape)
    else
      redirect_to edit_mixtape_path(@mixtape)
   	end
   end
   
   def destroy
     if @song.destroy
     redirect_to edit_mixtape_path(@mixtape)
   end
   end



private

    def song_params
     params.require(:song).permit(:title, :trackid, :audio) 	
    end   
    
    def find_mixtape
     @mixtape = Mixtape.find(params[:mixtape_id])
    end

    def find_song
      @mixtape = Mixtape.find(params[:mixtape_id])
      @song = @mixtape.songs.find(params[:id])
    end

end
