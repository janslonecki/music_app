class SearchController < ApplicationController

def results
    @search = Mixtape.search do
        fulltext params[:search]
    end
	@mixtapes = @search.results

end

end
