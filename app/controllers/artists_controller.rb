class ArtistsController < ApplicationController
before_action :find_artist, only: [:destroy]
impressionist :actions=>[:show]

def index
	@artists = Artist.all
end

def home
    @artists = Artist.all
    
end

def new
	@artist = Artist.new
end

def create
    @artist = Artist.new(artist_params)
    if @artist.save
    	redirect_to edit_artist_path(@artist)
    else
    	render 'new'
    end
end


def show
	@artist = Artist.find(params[:id])
    @mixtapes = @artist.mixtapes.all

end

def destroy
	@artist.destroy
    redirect_to root_path
end

def edit
    @artist = current_artist
    @mixtapes = @artist.mixtapes.all 
end

def update
    @artist = Artist.find(params[:id])
    if @artist.update(artist_params)
       redirect_to artist_path(@artist)
        else
        render 'new'
    end
end

def load_suggestions
  @suggestions = Artist.select(:title) or Artist.find(:all) #Select the data you want to load on the typeahead.
  render json: @suggestions
end


def upvote
    @artist = Artist.find(params[:id])
    @Artist.upvote_by current_user
    redirect_to artist_path(@artist)
end

def downvote
    @artist = Artist.find(params[:id])
    @Artist.downvote_by current_user
    redirect_to artist_path(@artist)
end


    private

        def artist_params
            params.require(:artist).permit(:name)
        end

        def find_mixtape
            @artist = Artist.find(params[:id])
        end

        def find_radio
            @artist = Artist.last
        end

end